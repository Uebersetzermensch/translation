# Sportvorschrift für das Heer
## A translation of the Wehrmacht Fitness Manual

### File Structure

---

* scan.pdf
  * The original scan of Sportvorschrift für das Heer
* transcription.pdf
  * A typed version of the original scan
* translation.txt
  * A translation of the transcription from german to english
* translation.md
  * A conversion of translation.txt into the markdown format
  * This is outdated, use translation.txt instead
* translation-latex
  * Contains LaTeX files for converting the translation into a finished PDF
  
### Current Status 1/01/20:
  
---

Theo has compiled a list of everything that needs to be retranslated.  Check out the editing and contribution sections below to help in the retranslation process.  If you need help or have a question, open an issue about it.

- [ ] p.12 sec. 8 paragraph 2 - retranslate "training of muscles and internal organs"
- [ ] p. 13 sec. 10 paragraph 1 - retranslate "it is generally mass instruction"
- [ ] p. 15 sec. III-12 - retranslate I and II
- [ ] p. 16-17 sec. 15-a - retranslate 
- [ ] p. 17-18 sec. 16-1 - retranslate
- [ ] p. 18 sec. 16-2-a - retranslate 
- [ ] p. 19 sec. 16-4 paragraph 3 - retranslate
- [ ] p. 20-21 sec. 17 - retranslate
- [ ] p. 22 sec. 18-b - retranslate
- [ ] p. 22 sec. 18-c - retranslate
- [ ] p. 23 sec. 20-a - retranslate
- [ ] p. 26 sec. 20-f - retranslate
- [ ] p. 26 sec. 20-g - retranslate
- [ ] p. 31-52 - retranslate, EXPERT NEEDED, complicated gymnastic exercise descriptions
- [ ] p. 57 second sentence - retranslate
- [ ] p. 57 sec. B first paragraph - retranslate
- [ ] p. 59 sec. 5 - retranslate
- [ ] p. 60 figure descriptions for figs 67, 68, 69 - retranslate
- [ ] P. 67 after fig 70 - retranslate
- [ ] P. 63, sec. E - Retranslate text under E
- [ ] P. 64 - Retranslate "In the beginning help comes from the teacher, later a trained person" Teachers BTFO
- [ ] P. 69 - retranslate, only about two sentences
- [ ] P. 70-72 - Retranslate, about 6 sentences total
- [ ] P. 76, sec. 4 - Retranslate ""Head on the breast part of the pike roll""
- [ ] P. 77 - We need a better word/phrase for "Ringswall", I don't know what that is and there's not an illustration
- [ ] P. 79, sec. 4 - retranslate "The runner recurs speed repeatedly"
- [ ] P. 81 - Retranslate commands "On the squares", "Done" - In context I think it is equivalent to ready, set , go
- [ ] P. 82 sec. F - Retranslate
- [ ] P. 95, sec. C, Sentence 3 - Retranslate sentence beginning with "Drifitng…"
- [ ] P. 96-97 - Retranslate, about two paragraphs total
- [ ] P. 100, sec. 44 - retranslate
- [ ] P. 107, sec 1 - retranslate sentence beginning with "backwards stop…"
- [ ] P. 108, sec. 4 - confirm translation of "intermediate"
- [ ] P. 122 - retranslate last sentence


### Editing Guide

---

* Only make an edit if a sentence can be made to sound better without changing its meaning.
* If a sentence sounds very off and you question its translation, open an issue about it.
* See "How to Contribute" below

### How to Contribute

---

To edit on GitLab(the easier way):

1. Click on translation.txt
2. Click edit
3. Make your changes
3. When finished, send your changes as a merge request

To edit locally:

1. Make an account on GitLab
2. Fork this repository by clicking the fork button at the top of the page
3. Make sure git is installed to your computer, [https://git-scm.com](https://git-scm.com)
4. Open the command line and navigate to the folder you want to work from (`cd <working directory>`)
5. Clone your repository to your computer (`git clone <link to your fork>`)
6. Add this repo to your local repo so you can fetch new updates (`git remote add upstream https://gitlab.com/sportvorschrift-fur-das-heer/translation` then `git fetch upstream`)
7. Create a new branch (`git checkout -b <name of your branch, i.e. what you're changing>`)
8. Make your changes
9. Stage your changes (`git add .`)
10. Commit your changes (`git commit -m "<what you changed>"`)
12. Update from the upstream (`git pull upstream master`)
11. Push your changes to GitLab (`git push origin`)
12. Return to your GitLab repo and open a merge request with your branch to have your changes reviewed and added to the main repo

Do not include **<** and **>** in your commands.

### The Plan

---

#### Phase 1: Collection of Material - Complete

* Rip PNGs of Images
* Transcribe the text into an unaltered German version

#### Phase 2: Elaboration of Material

* Clean up/redraw PNGs
* Translate the German Text
* Design a cover for the book

#### Phase 3: Profit

* Put everything together into the best book /pol/ has ever produced
